// Copyright (c) 2012, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

library widget;

import 'dart:html';

class Widget {
  final List<Widget> children = <Widget>[];
  Widget();

  void runApp() {
    walk(Widget widget) {
      var element = widget.render();

      for (var child in widget.children) {
        element.nodes.add(walk(child));
      }

      return element;
    }

    document.body.nodes.add(walk(this));
  }

  Element render() => new Element.html('<div></div>');
}
