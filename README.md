## About

Widget is an absolutely minimal UI framework in Dart. Like, really minimal.
Like, it's not actually a framework. It's just something I slapped together to
give a demo of the package system. It will go away when the demo is done.
Nothing to see here...

## Usage

Add widget to your `pubspec` like so:

    dependencies:
      widget:
        git: https://bitbucket.org/munificent/widget.git

Then import it:

    #import('package:widget/widget.dart');

Then build a widget hierarchy:

    var root = new Widget();
    root.children.add(new Widget());
    root.children.add(new Widget());
    root.children.add(new Widget());

Once that's done, start the app:

    root.runApp();
